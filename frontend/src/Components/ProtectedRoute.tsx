import React from 'react';
import {Route, Redirect} from 'react-router-dom';

const ProtectedRoute =
    ({component: Component, isAuthenticated, ...rest}: any) => {
      const routeComponent = ( ) => (
          isAuthenticated ?
          <Component {...rest} />:
          <Redirect to={{pathname: '/login'}}/>
      );
      return <Route {...rest} render={routeComponent}/>;
    };

export default ProtectedRoute;
