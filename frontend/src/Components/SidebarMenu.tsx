import React from 'react';
import {Divider, Drawer, List,
  ListItem, ListItemIcon, ListItemText,
} from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import clsx from 'clsx';

interface ISidebarMenuProps {
  classes : any
}

const ListItemLink = (props:any) => {
  return <ListItem button component="a" {...props} />;
};

const mainListItems = (
  <div>
    <ListItemLink href="/patients/add/">
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Add Patients" />
    </ListItemLink>
    <ListItemLink href="/">
      <ListItemIcon>
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="View Patients" />
    </ListItemLink>
  </div>
);

const SidebarMenu = (props: ISidebarMenuProps) => {
  return <Drawer
    variant="permanent"
    classes={{
      paper: clsx(props.classes.drawerPaper,
          false),
    }}
    open={true}
  >
    <div className={props.classes.toolbarIcon}>
    </div>
    <Divider />
    <List>{mainListItems}</List>
    <Divider />
  </Drawer>;
};

const sidebarMenu = SidebarMenu;
export default sidebarMenu;

