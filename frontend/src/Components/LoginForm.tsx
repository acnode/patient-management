import React, {useState} from 'react';
import {login} from './Authentication';
import {useHistory} from 'react-router-dom';
import {Button, Container, TextField, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
interface ILoginFormProps {
  changeLogin: (status: boolean) => void,
  isAuthenticated: boolean
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginForm = (props: ILoginFormProps) => {
  const [username, setusername] = useState('');
  const [password, setpassword] = useState('');
  const history = useHistory();
  const classes = useStyles();

  const handleLogin = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Make the API call
    await login(username, password);
    if (!props.isAuthenticated) {
      props.changeLogin(true);
      history.push('/');
    }
  };

  const handleChange = (event: React.FormEvent<HTMLInputElement
    | HTMLTextAreaElement>) => {
    const name = (event.target as HTMLInputElement).name;
    const value = (event.target as HTMLInputElement).value;
    if (name === 'password') {
      setpassword(value);
    } else {
      setusername(value);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} onSubmit={handleLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            label="Username"
            type="text"
            name="username"
            value={username}
            fullWidth
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            label="Password"
            type="password"
            name="password"
            value={password}
            fullWidth
            onChange={handleChange}
          />
          <Button
            variant="contained"
            fullWidth
            type="submit"
            color="primary"
            className={classes.submit}>
            Log In
          </Button>
        </form>
      </div>
    </Container>);
};

const loginForm = LoginForm;
export default loginForm;
