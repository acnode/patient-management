import axios from 'axios';

/**
 * Login
 * @param {string} username The username
 * @param {string} password The password
 * @return {void} Nothing
 */
export async function login(username: string, password: string): Promise<void> {
  const data: any = {username: username, password: password};
  await axios.post('/api-token-auth/', data)
      .then((res) => {
        localStorage.setItem('token', res.data.token);
      });
};

/**
 * IsAuthenticated
 * @return {boolean} of whether we have a token
 */
export function isAuthenticated() : boolean {
  const logged: boolean = localStorage.getItem('token') ? true : false;
  return logged;
};

/**
 * Logout
 * @return {void} Nothing
 */
export function logout() : void{
  localStorage.removeItem('token');
};
