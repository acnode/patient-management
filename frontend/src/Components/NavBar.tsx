import React from 'react';
import clsx from 'clsx';
import {AppBar, Button, InputBase,
  Toolbar, Typography} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

interface INavbarProps {
  onSearch: ( event: object ) => void
  loggedIn: boolean,
  classes : any
}

const NavBar = (props: INavbarProps) => {
  // Logged out
  const loggedOutNav = (
    <Button color="inherit" href="/login"><strong>Login</strong></Button>
  );

  // Logged in
  const loggedInNav = (
    <Button color="inherit" href="/logout"><strong>Logout</strong></Button>
  );

  // Return the Nav
  return (
    <AppBar position="absolute"
      className={clsx(props.classes.appBar,
          true && props.classes.appBarShift)}>
      <Toolbar className={props.classes.toolbar}>
        <Typography component="h1"
          variant="h6"
          color="inherit"
          noWrap
          className={props.classes.title}>
            Private Practice Management
        </Typography>
        {
        props.loggedIn ?
                   <div className={props.classes.search}>
                     <div className={props.classes.searchIcon}>
                       <SearchIcon />
                     </div>
                     <InputBase
                       placeholder="Search Patients…"
                       classes={{
                         root: props.classes.inputRoot,
                         input: props.classes.inputInput,
                       }}
                       onChange={props.onSearch}
                       inputProps={{'aria-label': 'search'}}
                     />
                   </div> : null
        }

        <div className="Auth">
          {props.loggedIn ? loggedInNav : loggedOutNav}
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
