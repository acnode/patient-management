import React from 'react';
import {logout} from './Authentication';
import {Redirect} from 'react-router-dom';

interface ILogoutProps {
  changeLogin: (status: boolean) => void,
  isAuthenticated: boolean
}

const LogoutPage = (props: ILogoutProps) => {
  if (props.isAuthenticated) {
    logout();
    props.changeLogin(false);
  }
  return <Redirect to={{pathname: '/login'}}/>;
};

export default LogoutPage;
