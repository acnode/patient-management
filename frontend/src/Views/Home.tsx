import React, {useEffect, useState} from 'react';
import Footer from '../Components/Footer';
import NavBar from '../Components/NavBar';
import {Router, Switch, Route} from 'react-router-dom';
import ProtectedRoute from '../Components/ProtectedRoute';
import {createBrowserHistory} from 'history';
import PatientList from './PatientsList';
import LoginForm from '../Components/LoginForm';
import LogoutPage from '../Components/Logout';
import '../App.css';
import {isAuthenticated} from '../Components/Authentication';
import Patient from './Patient';
import SidebarMenu from '../Components/SidebarMenu';
import axios from 'axios';
import {ToastProvider} from 'react-toast-notifications';
import Details from './Details';
import {fade, makeStyles} from '@material-ui/core/styles';
import {Box, Container, Grid} from '@material-ui/core';

const history = createBrowserHistory();

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  search: {
    'position': 'relative',
    'flexGrow': 1,
    'borderRadius': theme.shape.borderRadius,
    'backgroundColor': fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    'marginRight': theme.spacing(2),
    'marginLeft': 0,
    'width': '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '50em',
    },
  },
}));

/**
 * Home
 * @return {void} The Home view.
 */
const Home = () => {
  const [loggedIn, setLoggedIn] = useState(isAuthenticated());
  const [patientList, setPatients] = useState([]);
  const classes = useStyles();
  const handleSearch = (event?: any | undefined) => {
    getPatients(event.target.value)
        .then((result) => {
          setPatients(result);
          history.push('/');
        });
  };

  useEffect(() => {
    if (isAuthenticated()) {
      getPatients('')
          .then((result) => {
            setPatients(result);
          });
    }
  }, [loggedIn]);


  return (
    <div className={classes.root}>
      <ToastProvider placement="bottom-right">
        <NavBar
          loggedIn={loggedIn}
          onSearch={handleSearch}
          classes={classes}
        />
        { loggedIn ? <SidebarMenu classes={classes} /> :
            null
        }
        <main className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container maxWidth="lg" className={classes.container}>
            <Grid container spacing={3}>
              <Router history={history}>
                <Switch>
                  <ProtectedRoute exact path="/"
                    component={PatientList }
                    isAuthenticated={loggedIn}
                    patientList={patientList}
                    classes={classes}/>
                  <ProtectedRoute exact path="/patients/add"
                    component={Patient}
                    isAuthenticated={loggedIn}
                    title={'New Patient'}
                    classes={classes}/>
                  <ProtectedRoute path="/patients/edit/:patientId/"
                    component={Patient}
                    isAuthenticated={loggedIn}
                    title={'Edit Patient'}
                    classes={classes}/>
                  <ProtectedRoute path="/patients/view/:patientId/"
                    component={Details}
                    isAuthenticated={loggedIn}
                    classes={classes}/>
                  <Route exact path="/logout" >
                    <LogoutPage
                      changeLogin={setLoggedIn}
                      isAuthenticated={loggedIn}/>
                  </Route>
                  <Route>
                    <LoginForm
                      changeLogin={setLoggedIn}
                      isAuthenticated={loggedIn}/>
                  </Route>
                </Switch>
              </Router>
            </Grid>
            <Box pt={4}>
              <Footer />
            </Box>
          </Container>
        </main>
      </ToastProvider>
    </div>
  );
};

/**
 * getPatients
 * @param {string} search The search parameter
 * @return {Promise<any>} all the data specified by search
 */
async function getPatients(search: string | undefined): Promise<any> {
  let searchString ='';
  if (search) {
    searchString = '?search='+search;
  }
  const result = await axios.get('/api/patients/'+searchString, {
    headers: {
      'Authorization': `Token ${localStorage.getItem('token')}`,
    },
  });
  return result.data;
};

export default Home;
