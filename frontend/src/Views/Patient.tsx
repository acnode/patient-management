import React, {useEffect, useState} from 'react';
import {
  Stack,
  IStackProps, DatePicker, mergeStyleSets
  , IDatePickerStrings,
  IStackTokens,
} from 'office-ui-fabric-react';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import {withRouter, RouteComponentProps} from 'react-router';
import {useToasts} from 'react-toast-notifications';
import {Button, Grid, Paper, TextField} from '@material-ui/core';

const DayPickerStrings: IDatePickerStrings = {
  months: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],

  shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May',
    'Jun', 'Jul', 'Aug', 'Sep',
    'Oct', 'Nov', 'Dec'],

  days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday',
    'Thursday', 'Friday', 'Saturday'],

  shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],

  goToToday: 'Go to today',
  prevMonthAriaLabel: 'Go to previous month',
  nextMonthAriaLabel: 'Go to next month',
  prevYearAriaLabel: 'Go to previous year',
  nextYearAriaLabel: 'Go to next year',
  closeButtonAriaLabel: 'Close date picker',
};

interface INewPatientProps extends RouteComponentProps {
  title: string;
  classes: any;
}

interface IPatient {
  id: string | undefined;
  dateOfBirth: any;
  fileNumber :string | undefined;
  name :string | undefined;
  phoneNumber :string | undefined;
  phoneNumberAlt :string | undefined;
  email :string | undefined;
  occupation :string | undefined;
  employer :string | undefined;
  nextOfKinName :string | undefined;
  nextOfKinNameContact :string | undefined;
  nextOfKinNameRelationShip :string | undefined;

}

const Patient = (props: INewPatientProps) => {
  const {match} = props;
  const params: any = match.params;
  const patientId :any = params.patientId;
  const isNew: boolean = patientId === undefined ? true : false;
  const history = useHistory();
  const {addToast} = useToasts();
  const [dateOfBirth, setDateOfBirth] = useState({date: undefined});
  const [fileNumber, setFileNumber] = useState('');
  const [name, setName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [phoneNumberAlt, setPhoneNumberAlt] = useState('');
  const [email, setEmail] = useState('');
  const [occupation, setOccupation] = useState('');
  const [employer, setEmployer] = useState('');
  const [nextOfKinName, setNextOfKinName] = useState('');
  const [nextOfKinNameContact, setNextOfKinNameContact] = useState('');
  const [nextOfKinNameRelationShip, setNextOfKinNameRelationShip] =
    useState('');

  // on mount
  useEffect(() => {
    if (!isNew) {
      axios.get('/api/patients/'+ patientId+'/', {
        headers: {
          'Authorization': `Token ${localStorage.getItem('token')}`,
        },
      }).then((response) => {
      // date conversion shenanigans
        const dob: string = response.data.dateOfBirth;
        // @ts-ignore
        setDateOfBirth({date: new Date(dob)});
        setFileNumber(response.data.fileNumber);
        setName(response.data.name);
        setPhoneNumber(response.data.phoneNumber);
        setEmail(response.data.email);
        setOccupation(response.data.occupation);
        setEmployer(response.data.employer);
        setNextOfKinName(response.data.nextOfKinName);
        setNextOfKinNameContact(response.data.nextOfKinNameContact);
        setNextOfKinNameRelationShip(response.data.nextOfKinNameRelationShip);
      });
    }
  }, [patientId, isNew]);

  // on submistting
  const submitAction = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const patient : IPatient ={
      id: patientId,
      dateOfBirth: dateOfBirth.date,
      email: email,
      employer: employer,
      fileNumber: fileNumber,
      name: name,
      nextOfKinName: nextOfKinName,
      nextOfKinNameContact: nextOfKinNameContact,
      nextOfKinNameRelationShip: nextOfKinNameRelationShip,
      occupation: occupation,
      phoneNumber: phoneNumber,
      phoneNumberAlt: phoneNumberAlt,
    };
    savePatient(isNew, patient)
        .then((patient) => {
          addToast('Saved Successfully', {appearance: 'success'});
          history.push('/patients/view/'+patient.id+'/');
        })
        .catch((error) => {
          addToast(JSON.stringify(error.response.data), {appearance: 'error'});
        });
  };

  // column styling
  const columnProps: Partial<IStackProps> = {
    tokens: {childrenGap: 15},
    styles: {root: {width: 300}},
  };

  // stack styling
  const headingStackTokens: IStackTokens = {childrenGap: 50};

  // control class styling
  const controlClass = mergeStyleSets({
    control: {
      margin: '0 0 15px 0',
      maxWidth: '300px',
    },
  });

  // handler for date selection
  const onSelectDate = (date: Date | null | undefined): void => {
    // @ts-ignore we should be able to assing
    setDateOfBirth({date: date});
  };

  // return
  return (
    <Grid item xs={12}>
      <Paper className={props.classes.paper}>
        <form onSubmit={(e) => {
          return submitAction(e);
        }}>
          <h1>{props.title}</h1>
          <Stack horizontal disableShrink tokens={headingStackTokens}>
            <Stack {...columnProps}>
              <TextField
                label="File Number"
                type="text"
                name="fileNumber"
                required={true}
                variant="outlined"
                onChange={(event) => {
                  setFileNumber((event.target as HTMLInputElement).value);
                }}
                value={fileNumber}
              />
              <TextField
                label="Patient Name"
                type="text"
                name="name"
                required={true}
                variant="outlined"
                onChange={(event) => {
                  setName((event.target as HTMLInputElement).value);
                }}
                value={name}
              />
              <TextField
                label="Phone Number"
                type="text"
                name="phoneNumber"
                variant="outlined"
                required={true}
                onChange={(event) => {
                  setPhoneNumber((event.target as HTMLInputElement).value);
                }}
                value={phoneNumber}
              />
              <DatePicker
                className={controlClass.control}
                label="Date of Birth"
                isRequired={true}
                strings={DayPickerStrings}
                placeholder="Select a date..."
                ariaLabel="Select a date"
                onSelectDate={onSelectDate}
                value={dateOfBirth.date}
              />
            </Stack>
            <Stack {...columnProps}>
              <TextField
                label="Alternative Phone"
                type="text"
                name="phoneNumberAlt"
                variant="outlined"
                onChange={(event) => {
                  setPhoneNumberAlt((event.target as HTMLInputElement).value);
                }}
                value={phoneNumberAlt}
              />
              <TextField
                label="Email"
                type="email"
                name="email"
                variant="outlined"
                onChange={(event) => {
                  setEmail((event.target as HTMLInputElement).value);
                }}
                value={email}
              />
              <TextField
                label="Occupation"
                type="text"
                name="occupation"
                variant="outlined"
                onChange={(event) => {
                  setOccupation((event.target as HTMLInputElement).value);
                }}
                value={occupation}
              />
              <TextField
                label="Employer"
                type="text"
                name="employer"
                variant="outlined"
                onChange={(event) => {
                  setEmployer((event.target as HTMLInputElement).value);
                }}
                value={employer}
              />
            </Stack>
            <Stack {...columnProps}>
              <TextField
                label="Next Of Kin Name"
                type="text"
                name="nextOfKinName"
                required={false}
                variant="outlined"
                onChange={(event) => {
                  setNextOfKinName((event.target as HTMLInputElement).value);
                }}
                value={nextOfKinName}
              />
              <TextField
                label="Next Of Kin Contact"
                type="text"
                name="nextOfKinNameContact"
                variant="outlined"
                onChange={(event) => {
                  setNextOfKinNameContact(
                      (event.target as HTMLInputElement).value);
                }}
                value={nextOfKinNameContact}
              />
              <TextField
                label="Next Of Kin Relationship"
                type="text"
                name="nextOfKinNameRelationShip"
                variant="outlined"
                onChange={(event) => {
                  setNextOfKinNameRelationShip(
                      (event.target as HTMLInputElement).value);
                }}
                value={nextOfKinNameRelationShip}
              />
              <Stack.Item align="end">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"> Save Record </Button>
              </Stack.Item>

            </Stack>

          </Stack>
        </form>

      </Paper>
    </Grid>);
};

/**
 * savePatient
 * @param {boolean} isNew Whether we are adding or editing
 * @param {any} data The patient we are adding or editing
 * @return {Promise<any>} all the data specified by addition
 */
async function savePatient(isNew: boolean, data: any) : Promise<any> {
  let result;
  if (data.dateOfBirth) {
    data.dateOfBirth = data.dateOfBirth.toISOString().substr(0, 10);
  }

  if (isNew) {
    // we are adding and need to do a post
    result = await axios.post('/api/patients/', data, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    });
    return result.data;
  } else {
    // we are modifying and need to do a patch
    result = await axios.patch('/api/patients/'+ data.id+'/', data, {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    });
    return result.data;
  }
}

const patient = Patient;
export default withRouter(patient);
