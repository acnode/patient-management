import React, {useState} from 'react';
import {
  Button, Grid, Paper,
  Table, TableBody, TableCell,
  TableContainer, TableHead, TablePagination,
  TableRow,
} from '@material-ui/core';

interface IPatientListProps {
  patientList : any [] // TODO type me
  classes : any
}

const PatientList = (props : IPatientListProps) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(20);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage =
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };

  return (
    <Grid item xs={12}>
      <Paper className={props.classes.paper}>
        <h1>Patient List</h1>
        <TableContainer component={Paper}>
          <Table className={props.classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell>Patient Name</TableCell>
                <TableCell>File Number</TableCell>
                <TableCell align="right">Mobile</TableCell>
                <TableCell align="right">Mobile 2</TableCell>
                <TableCell align="right">Email</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.patientList
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row) => {
                    const hrefLink = `/patients/view/${row.id}/`;
                    return (
                      <TableRow key={row.id}>
                        <TableCell align="left">{row.id}</TableCell>
                        <TableCell component="th" scope="row">
                          {row.name}
                        </TableCell>
                        <TableCell align="left">{row.fileNumber}</TableCell>
                        <TableCell align="right">{row.phoneNumber}</TableCell>
                        <TableCell align="right">
                          {row.phoneNumberAlt}
                        </TableCell>
                        <TableCell align="right">{row.email}</TableCell>
                        <TableCell align="right">
                          <Button variant="contained"
                            color="primary"
                            href={hrefLink}>Details
                          </Button>
                        </TableCell>
                      </TableRow>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[20, 50]}
          component="div"
          count={props.patientList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </Grid>);
};

const patientList = PatientList;
export default patientList;
