import React, {useEffect, useState} from 'react';
import {RouteComponentProps, withRouter} from 'react-router';
import axios from 'axios';
import {Button, Grid, Paper, Typography} from '@material-ui/core';

interface IDetailsProps extends RouteComponentProps {
  classes: any
}

const Details = (props: IDetailsProps) => {
  const {match} = props;
  const params: any = match.params;
  const patientId :any = params.patientId;
  const [patient, setPatient] = useState({
    name: undefined,
    email: undefined,
    fileNumber: undefined,
    phoneNumber: undefined,
    phoneNumberAlt: undefined,
    dateOfBirth: undefined,
    occupation: undefined,
    nextOfKinName: undefined,
    nextOfKinNameContact: undefined,
    employer: undefined,
    nextOfKinNameRelationShip: undefined,
  });

  useEffect(() => {
    axios.get('/api/patients/'+ patientId+'/', {
      headers: {
        'Authorization': `Token ${localStorage.getItem('token')}`,
      },
    }).then((response) => {
      setPatient(response.data);
    });
  }, [patientId]);

  const editLink:string = `/patients/edit/${patientId}/`;

  return (
    <Grid item xs={12}>
      <Paper className={props.classes.paper} elevation={3}>
        <div>
          <Typography variant="h3" component="h3" gutterBottom>
          Patient Details
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Email: </strong>{patient.email}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>File Number: </strong>{patient.fileNumber}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Phone Number: </strong>{patient.phoneNumber}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Phone Number Alt: </strong>{patient.phoneNumberAlt}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Date of Birth: </strong>{patient.dateOfBirth}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Occupation : </strong>{patient.occupation}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Employer : </strong>{patient.employer}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Next of Kin Name: </strong>{patient.nextOfKinName}
          </Typography>
          <Typography variant="h6" component="h3">
            <strong>Next of Kin Contact: </strong>
            {patient.nextOfKinNameContact}
          </Typography>
          <Typography variant="h6" component="h3" gutterBottom>
            <strong>Next of Kin Relationship: </strong>
            {patient.nextOfKinNameRelationShip}
          </Typography>
          <Button
            variant="contained"
            color="primary"
            href={editLink}>
          Edit Patient
          </Button>
        </div>
      </Paper>
    </Grid>);
};

const detail = Details;
export default withRouter(detail);
