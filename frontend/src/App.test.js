import React from 'react';
import {render} from '@testing-library/react';
import Home from './Views/Home';

test('renders learn react link', () => {
  const {getByText} = render(<Home />);
  const linkElement = getByText(/Private/i);
  expect(linkElement).toBeInTheDocument();
});
