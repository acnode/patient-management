"""
Apps
"""
from django.apps import AppConfig


class DaktariConfig(AppConfig):
    """
    Configuration for the App
    """
    name = 'daktari'
