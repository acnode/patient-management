"""
Serializers
"""
from rest_framework import serializers
from .models import Patient


class PatientSerializer(serializers.ModelSerializer):
    """
    Serializer for Patient
    """
    class Meta:
        """
        Meta for Patient
        """
        model = Patient
        fields = '__all__'
