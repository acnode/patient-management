"""
Create your views here.
"""

from .models import Patient
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
import logging
import os
from django.views.generic import View
from django.http import HttpResponse
from django.conf import settings
from rest_framework import filters

from .serializers import PatientSerializer


class FrontendAppView(View):
    """
    Serves the compiled frontend entry point (only works if you have run `yarn
    run build`).
    """

    def get(self, request):
        print(os.path.join(settings.REACT_APP_DIR, 'build', 'index.html'))
        try:
            with open(os.path.join(settings.REACT_APP_DIR, 'build', 'index.html')) as f:
                return HttpResponse(f.read())
        except FileNotFoundError:
            logging.exception('Production build of app not found')
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )


class PatientView(viewsets.ModelViewSet,):
    """
    The Patient View. By default can list and create
    """
    search_fields = ['email', 'name', 'fileNumber', 'phoneNumber', 'phoneNumberAlt', 'nextOfKinName',
                     'nextOfKinNameContact', 'occupation', 'employer']
    filter_backends = (filters.SearchFilter,)
    permission_classes = (IsAuthenticated,)
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
