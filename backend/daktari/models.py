"""
Create your models here.
"""
from django.db import models


class Patient(models.Model):
    """
    The patient Model
    """
    fileNumber = models.CharField(max_length=20)
    name = models.CharField(max_length=120)
    phoneNumber = models.CharField(max_length=120)
    phoneNumberAlt = models.CharField(max_length=120, blank=True, null=True)
    dateOfBirth = models.DateField()
    email = models.EmailField(blank=True, null=True)
    occupation = models.CharField(max_length=120, blank=True, null=True)
    employer = models.CharField(max_length=120, blank=True, null=True)
    nextOfKinName = models.CharField(max_length=120, blank=True, null=True)
    nextOfKinNameContact = models.CharField(max_length=120, blank=True, null=True)
    nextOfKinNameRelationShip = models.CharField(max_length=120, blank=True, null=True)

    def _str_(self):
        # String representation of a Patient
        return self.name
