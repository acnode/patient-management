"""
Create your test cases here.
"""
from django.test import TestCase
from .models import Patient
from datetime import datetime


class PatientTestCase(TestCase):
    """
    Create your test here.
    """

    def test_patient_file_number(self):
        Patient.objects.create(
            fileNumber="1",
            name="Andrew Eastman",
            phoneNumber="0722844920",
            phoneNumberAlt="",
            dateOfBirth=datetime(1992, 10, 26),
            email="andrueastman@gmail.com"
        )
        patient = Patient.objects.get(name="Andrew Eastman")
        self.assertEqual(patient.fileNumber, '1')
