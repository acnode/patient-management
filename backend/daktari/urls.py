from django.conf.urls import url
from django.urls import include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'patients', views.PatientView, basename='patients')

urlpatterns = [
    url(r'^', include(router.urls)),
]
