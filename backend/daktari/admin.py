"""
Register your models here.
"""
from django.contrib import admin
from .models import Patient


class DaktariAdmin(admin.ModelAdmin):
    """
    The Daktari Admin
    """
    list_display = ('fileNumber', 'name', 'phoneNumber')


admin.site.register(Patient, DaktariAdmin)
