# Generated by Django 3.0.6 on 2020-05-30 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('daktari', '0003_auto_20200530_1714'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patient',
            name='phoneNumberAlt',
            field=models.CharField(blank=True, max_length=120, null=True),
        ),
    ]
