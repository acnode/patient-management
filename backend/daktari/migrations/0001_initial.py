# Generated by Django 3.0.6 on 2020-05-30 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fileNumber', models.CharField(max_length=20)),
                ('name', models.CharField(max_length=120)),
                ('phoneNumber', models.CharField(max_length=120)),
                ('phoneNumberAlt', models.CharField(max_length=120)),
                ('dateOfBirth', models.DateField()),
                ('email', models.EmailField(max_length=254)),
                ('occupation', models.CharField(max_length=120)),
                ('employer', models.CharField(max_length=120)),
                ('nextOfKinName', models.CharField(max_length=120)),
                ('nextOfKinNameContact', models.CharField(max_length=120)),
                ('nextOfKinNameRelationShip', models.CharField(max_length=120)),
            ],
        ),
    ]
