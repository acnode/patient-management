#!/bin/bash
echo "We are In"
echo "Updating system"
apt-get -y update
apt-get -y upgrade
echo "Installing dependencies"
apt-get -y install python python3-pip nodejs wget unzip python-virtualenv nginx supervisor build-essential libpq-dev python-dev
echo 'Installing Python Dependencices'
pkill -f "python"
pip install -r requirements.txt
pip install gunicorn
echo 'Remove old data'
rm -r artifacts.*
echo 'Attempting to download build artifacts'
wget -O artifact.zip https://gitlab.com/acnode/patient-management/-/jobs/artifacts/master/download?job=production_build
echo 'Checking folder structure'
ls -la
echo 'Unzipping'
unzip -qq -o artifact.zip
echo 'Checking folder structure'
ls -la
echo 'Navigating to Folder'
cd backend
echo 'Running migrations'
python3 manage.py migrate
echo 'Collecting static'
python3 manage.py collectstatic --noinput
echo 'Running server'
virtualenv venv
source venv/bin/activate
pip install -r ../requirements.txt
pip install gunicorn
chmod -R 755 -R ../
sudo systemctl restart supervisor
sudo systemctl restart nginx